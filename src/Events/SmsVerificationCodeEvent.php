<?php

namespace Myaccountl\LaravelTools\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SmsVerificationCodeEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $phone = null;
    public $code = null;

    /**
     * Create a new event instance.
     *
     * @param $phone
     * @param string $code 验证码
     */
    public function __construct($phone, $code)
    {
        //
        $this->phone = $phone;
        $this->code = $code;
    }

}
