<?php


namespace Myaccountl\LaravelTools;


class Rsa
{
    /**
     * 生成RsaKey
     * @param null $config
     * @return array
     */
    public static function getRsaKeys($config=null){
        $config_ = [
            "digest_alg" => "sha512",
            "private_key_bits" =>1024,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
            "config" => config('rsa.openssl.config.path')
        ];
        if($config){
            $config_ = array_merge($config_, $config);
        }
        //创建密钥对
        $res = openssl_pkey_new($config_);
        //生成私钥
        openssl_pkey_export($res, $privkey, null, $config_);
        //生成公钥
        $pubKey = openssl_pkey_get_details($res)['key'];
        return ['private' => $privkey, 'public' => $pubKey];
    }
    /*-----------------------------  公钥加密, 私钥解密 --------------------------------------*/
    /*
     * RSA公钥加密
     * 使用私钥解密
     */
    public static function enRSA_public($publicKey, $aString) {
        $pu_key = openssl_pkey_get_public($publicKey);//这个函数可用来判断公钥是否是可用的
        openssl_public_encrypt($aString, $encrypted, $pu_key);//公钥加密，私钥解密
        $encrypted = base64_encode($encrypted);//加密后的内容通常含有特殊字符，需要编码转换下，在网络间通过url传输时要注意base64编码是否是url安全的
        return $encrypted;
    }
    /*
     * RSA私钥解密
     * 有可能传过来的aString是经过base64加密的，则传来前需先base64_decode()解密
     * 返回未经base64加密的字符串
     */
    public static function deRSA_private($privateKey, $aString) {
        $pr_key = openssl_pkey_get_private($privateKey);//这个函数可用来判断私钥是否是可用的
        openssl_private_decrypt(base64_decode($aString), $decrypted, $pr_key);//公钥加密，私钥解密
        return $decrypted;
    }

    /*-----------------------------  私钥加密, 公钥解密 --------------------------------------*/
    /*
     * RSA私钥加密
     * 加密一个字符串，返回RSA加密后的内容
     * aString 需要加密的字符串
     * return encrypted rsa加密后的字符串
     */
    public static function enRSA_private($privateKey, $aString) {
        //echo "------------",$aString,"====";
        $pr_key = openssl_pkey_get_private($privateKey);//这个函数可用来判断私钥是否是可用的，可用返回资源id Resource id
        openssl_private_encrypt($aString, $encrypted, $pr_key);//私钥加密
        $encrypted = base64_encode($encrypted);//加密后的内容通常含有特殊字符，需要编码转换下，在网络间通过url传输时要注意base64编码是否是url安全的
        //echo "加密后:",$encrypted,"\n";
        return $encrypted;
    }
    /*
     * RSA公钥解密
     */
    public static function deRSA_public($publicKey, $aString) {
        $pu_key = openssl_pkey_get_public($publicKey);//这个函数可用来判断公钥是否是可用的
        openssl_public_decrypt(base64_decode($aString), $decrypted, $pu_key);//公钥加密，私钥解密
        return $decrypted;
    }
}
