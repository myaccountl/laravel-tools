<?php

namespace Myaccountl\LaravelTools;

abstract class BaseServices
{
    use JsonResponse;

    /**
     * @return static
     */
    public static function make()
    {
        return new static();
    }
}
