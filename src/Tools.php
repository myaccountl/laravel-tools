<?php


namespace Myaccountl\LaravelTools;


use Myaccountl\LaravelTools\Events\SmsVerificationCodeEvent;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class Tools
{

    /**
     * 缓存处理
     * @param $key
     * @param $call_back
     * @param null $ttl
     * @return false|mixed|null
     */
    public static function cacheData($key, $call_back, $ttl=null)
    {
        $data = Cache::get($key);
        if ($data) {
            return $data;
        }
        $lock = Cache::lock($key . ':lock', 10);
        try {
            $lock->block(10);
            // 通过回调函数获取需要缓存的数据
            if ($call_back instanceof \Closure) {
                $data = call_user_func($call_back);
                if ($data) {
                    Cache::put($key, $data, $ttl);
                }
                return $data;
            }
        } catch (\Exception $e) {
        } finally {
            optional($lock)->release();
        }
        return $data;
    }


    /**
     * @param string $img 图片二进制数据
     * @return string
     */
    public static function imageEncodeBase64($img)
    {
        return 'data:image/png;base64,' . base64_encode($img);//转base64
    }

    /**
     * 登录次数限制
     * @param string $prefix 前缀
     * @param string $username 用户名
     * @param int $count 超过多少次进行限制
     * @param int $expire 过期时间（单位：秒）
     * @return bool
     */
    public static function loginLimitCheck($prefix, $username, $count, $expire=null)
    {
        $value = Redis::incr($prefix . $username);
        if ($expire) {
            Redis::expire($prefix . $username, $expire);
        }
        if ($value > $count) {
            return true;
        }
        return false;
    }


    /**
     * 登录次数清除
     * @param string $prefix
     * @param string $username
     * @return bool
     */
    public static function loginLimitClear($prefix, $username)
    {
        Redis::del($prefix . $username);
        return true;
    }

    /**
     * 利用laravel 的decrypt 函数进行解密，如果解密失败返回设定的默认值
     * @param string|null $string 加密字符串
     * @param null $default 默认值
     * @return mixed|null
     */
    public static function decrypt($string, $default=null){
        try {
            if(!$string){
                return $default;
            }
            return decrypt($string);
        }catch (\Illuminate\Contracts\Encryption\DecryptException $e){
            return $default;
        }
    }

    /**
     * 通过随机字符串和时间戳的方式获取一个随机的uuid
     * @return string
     * @throws \Exception
     */
    public static function uuid()
    {
//        $str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
//        $rand_str = microtime(true) . substr(str_shuffle($str), 0, 15);
//        return (string)Uuid::generate(5, $rand_str, Uuid::NS_DNS);
        return Str::orderedUuid()->toString();
    }

    /**
     * 生成sn
     * @param string $prefix 前缀
     * @return string
     */
    public static function sn($prefix='')
    {
        return $prefix . date('YmdHis') . mt_rand(10000, 100000);
    }

    /**
     * 验证是否是一个手机号
     * @param string $value
     * @return false|int
     */
    public static function is_phone(string $value){
        return (bool)preg_match('/^1([38]\d|4[579]|5[0-35-9]|66|7[035-8]|9[89])\d{8}$/', $value);
    }

    /**
     * 获取路径
     * @param $path
     * @return array|false|int|string|null
     */
    public static function getPath($path)
    {
        $path = parse_url($path, PHP_URL_PATH);
        if(Str::startsWith($path, '/upload/')){
            return mb_substr($path, 8);
        }
        if(Str::startsWith($path, '/')){
            return public_path($path);
        }
        return $path;
    }

    /**
     * 模拟发送短信验证码
     * @param string $to 接收短信的验证码
     * @param int $ttl 过期时间 单位: 秒 默认5分钟有效
     * @param bool $debug 是否调试模式 如果调试模式则不会正常发短信使用虚拟数字111111作为验证码
     * @return bool
     */
    public static function sendVerifyCode($to, $ttl=300, $debug=true)
    {
        $code = Cache::get($to);
        if($code){
            if(time() - $code['send_time'] < 60){
                return false;
            }
        }
        if($debug){
            $code = '111111';
        }else{
            // 生成随机数字并调用发短信接口 如果调用成功则存储到缓存
            $code = substr(str_shuffle('0123456789'), 0, 6);
            // 调用发送短信接口
            event(new SmsVerificationCodeEvent($to, $code));
        }
        Cache::put($to, ['code' => $code, 'send_time' => time()], $ttl);
        return true;
    }

    /**
     * 验证传入的验证码和手机号 是否和服务端的一致
     * @param string $phone
     * @param string $code
     * @return bool
     */
    public static function verifyPhoneCode($phone, $code)
    {
        $code_obj = Cache::get($phone);
        if ($code_obj){
            if((string)$code_obj['code'] == (string)$code){
                Cache::pull($phone);
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $value
     * @return false|int
     */
    public static function is_integer(string $value){
        return (bool)preg_match('/^\d+$/', $value);
    }

    /**
     * 通过传入的路径和文件后缀生成完整的文件保存路径
     * @param string $path 文件路径
     * @param string $extension 文件后缀
     * @param string $custom_filename 自定义文件名
     * @return string
     */
    public static function getFullName($disk, $path, $extension, $custom_filename='')
    {
        $disk = Storage::disk($disk);
        $path = $path . '/' . date( "Ymd" );
        if(!$disk->exists($path)){$disk->makeDirectory($path);}
        $file_name = $path . '/' . ($custom_filename?:Str::uuid()) . '.' . $extension;
        if (in_array('qiniu' , config('filesystems.disks'))) {
            $disk_qiniu = Storage::disk('qiniu');
            while ($disk->exists($file_name) || $disk_qiniu->exists('/storage/' . $file_name)) {
                $file_name = $path . '/' . Str::uuid() . '.' . $extension;
            }
        }
        return $file_name;
    }

    /**
     * 获取文件数据
     * @param string $path
     * @return string
     */
    public static function getFile($path)
    {
        try {
            if(Storage::disk('public')->exists($path)){
                return Storage::disk('public')->get($path);
            }
            $result = file_get_contents($path);
            if($result){
                return $result;
            }
        }catch (\Exception $exception){
        }
        return '';
    }

    /**
     * 进行文件存储
     * @param string $fileName 文件名称
     * @param string $content 文件内容
     * @return string 文件url
     */
    public static function fileSave($fileName, $content)
    {
        if(Storage::disk('public')->put($fileName, $content)){
            $qiniu_file = '/storage/' . $fileName;
            if (in_array('qiniu' , config('filesystems.disks'))) {
                if (Storage::disk('qiniu')->put($qiniu_file, $content)) {
                    return Storage::disk('qiniu')->url($qiniu_file);
                }
            } else {
                return Storage::disk('public')->url($fileName);
            }
        }
        return '';
    }

    /**
     * 生成二维码并返回url
     * @param $text
     * @param int $size 二维码大小
     * @param int $margin 二维码边框大小
     * @param string $custom_filename 自定义存储文件名
     * @param string $logo 要添加的logo
     * @param bool $resultBlob 是否返回二进制数据
     * @return mixed
     * @throws \Exception
     */
    public static function qrCode($text, $size=344, $margin=5, $custom_filename='', $logo='', $resultBlob=false){

        //创建二维码并保存二维码图片
        $qr = QrCode::format('png')
            ->size($size) // 设置二维码大小 据腾讯建议信息 二维码大小为43的倍数最好
            ->encoding('UTF-8')
            ->errorCorrection('H')
            ->margin($margin); // 添加边距
        if($logo){
            $data = static::createIcon($logo);
            $qr->mergeString($data, .2, true);
        }
        $data = $qr->generate($text);
        if($resultBlob){
            return $data;
        }
        //随机二维码图片名
        $fileName = static::getQrcodeFullName( $custom_filename);
        return static::fileSave($fileName, $data);
    }

    /**
     * 获取二维码地址全路径
     * @param $custom_filename
     * @return string
     */
    public static function getQrcodeFullName($custom_filename)
    {
        return static::getFullName('public', 'qrcode', 'png', $custom_filename);
    }


    /**
     * 生成用户邀请二维码
     * @param string $extend_uuid 推广ID
     * @param string $logo 二维码logo
     * @param bool $logoBlob 传入的二维码logo是否二进制数据
     * @return mixed|string 二维码链接
     * @throws \Exception
     */
    public static function createUserExtendQrCode($extend_uuid, $logo='', $logoBlob=false)
    {
        try{
            $paramLogo = $logoBlob?'二进制数据': $logo;
            if(empty($extend_uuid)){
                return '';
            }
            $qr_url = config('qrcode.content')?:route('user_extend_qrcode');
            $pos = strpos($qr_url, '?');
            $query = '';
            if ($pos !== false) {
                $query = substr($qr_url, $pos + 1);
                $qr_url = substr($qr_url, 0, $pos);
            }
            parse_str($query, $query_array);
            $query_array['invited'] = $extend_uuid;
            $qr_url .= '?' . http_build_query($query_array);
            if(!$logoBlob){
                $logo = static::getFile($logo)?:resource_path('images/logo.png');
            }
            return static::qrCode($qr_url, config('qrcode.size'), config('qrcode.margin'),$extend_uuid, $logo);
        }catch (\Exception $exception){
            Log::error(__FILE__ . ': ' . $exception->getMessage(), ['uuid' => $extend_uuid, 'logo' => $paramLogo, 'logoBlob' => $logoBlob]);
        }
        return '';
    }

    // 过滤掉emoji表情
    public static function filterEmoji($str)
    {
        $str = preg_replace_callback( '/./u',
            function (array $match) {
                return strlen($match[0]) >= 4 ? '' : $match[0];
            },
            $str);
        return $str;
    }

    function chararray($str,$charset="utf-8"){

        $re['utf-8']   = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";

        $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";

        $re['gbk']    = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";

        $re['big5']   = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";

        $data = explode(PHP_EOL, $str);
        $result = [];
        foreach ($data as $v){
            preg_match_all($re[$charset], $v, $new_match);
            array_push($result, ...$new_match[0]);
            array_push($result, PHP_EOL);
        }
//        preg_match_all($re[$charset], $str, $match);

        return $result;

    }

    /**
     * 给Imagick 设置文字水印
     * @param \Imagick $imagick
     * @param string $text
     * @param int $x
     * @param int $y
     * @param int $angle
     * @param array $style
     * @param int $width
     */
    public static function imagickAddText(\Imagick &$imagick, $text, $x = 0, $y = 0, $angle = 0, $style = array(), $width=258)
    {
        $text = static::filterEmoji($text);
        $draw = new \ImagickDraw ();
        if (isset ($style ['font']))
            $draw->setFont($style ['font']);

        if (isset ($style ['font_size']))
            $draw->setFontSize($style ['font_size']);

        if (isset ($style ['fill_color']))
            $draw->setFillColor($style ['fill_color']);

        if (isset ($style ['under_color']))
            $draw->setTextUnderColor($style ['under_color']);

        if (isset ($style ['font_family']))
            $draw->setfontfamily($style ['font_family']);

        if (isset ($style ['font_weight']))
            $draw->setFontWeight($style ['font_weight']);
        $draw->settextencoding('UTF-8');
        $_string = "";
        $_width  = 0;
        $temp = static::chararray($text);
        foreach ($temp as $k=>$v){
            if($v == PHP_EOL){
                $_width = 0;
                $_string .= PHP_EOL;
            }
            $w = $imagick->queryFontMetrics($draw, $v);
            $_width += intval($w['textWidth']);
            if (($_width > $width) && ($v !== "")){
                $_string .= PHP_EOL;
                $_width = 0;
            }
            $_string .= $v;
            unset($w);
        }
//        $draw->annotation($x, $y, trim($_string));
//        $imagick->drawImage($draw);
        if (strtolower($imagick->getImageFormat()) == 'gif') {
            foreach ($imagick as $frame) {
                $frame->annotateImage($draw, $x, $y, $angle, trim($_string));
            }
        } else {
            $imagick->annotateImage($draw, $x, $y, $angle, trim($_string));

        }
    }

    /**
     * 生成用户推广二维码 把二维码和推广背景合并起来
     * @param string $bg
     * @param string $qrcode
     * @param float $x
     * @param float $y
     * @param float $qr_width
     * @param float $qr_height
     * @param float $width
     * @param float $height
     * @return \Imagick
     * @throws \ImagickException
     */
    public static function extendQrCode($bg, $qrcode, $x, $y, $qr_width, $qr_height, $width=0.0, $height=0.0)
    {
        if($bg instanceof \Imagick){
            $bg_img = $bg;
        }else{
            $bg_img = $bg instanceof \Imagick ? $bg : new \Imagick();
            try {
                if(is_file($bg) || Str::startsWith($bg, 'http')){
                    $bg_img->readImage($bg);
                }else{
                    $bg_img->readImageBlob($bg);
                }
            }catch (\Exception $exception){
                try {
                    $bg_img->readImageBlob($bg);
                }catch (\Exception $exception){
                    $bg_img->destroy();
                    throw new \Exception('背景图片读取失败！');
                }
            }
        }
        if($qrcode instanceof \Imagick){
            $qrcode_img = $qrcode;
        }else{
            $qrcode_img = new \Imagick();
            try {
                if(is_file($qrcode) || Str::startsWith($qrcode, 'http')){
                    $qrcode_img->readImage($qrcode);
                }else{
                    $qrcode_img->readImageBlob($qrcode);
                }
            }catch (\Exception $exception){
                try {
                    $qrcode_img->readImageBlob($qrcode);
                }catch (\Exception $exception){
                    $qrcode_img->destroy();
                    $bg_img->destroy();
                    throw new \Exception('二维码读取失败！');
                }
            }
        }
        if($qrcode_img->getImageWidth() != $qr_width || $qrcode_img->getImageHeight() != $qr_height){
            $qrcode_img->scaleImage($qr_width, $qr_height, true);
        }
        $bg_img->compositeImage($qrcode_img, \Imagick::COMPOSITE_OVER, $x, $y);
        if($width && $height){
            $bg_img->scaleImage($width, $height);
        }
//        $result = $bg_img->getImageBlob();
        $qrcode_img->destroy();
//        $bg_img->destroy();
        return $bg_img;

    }

    /**
     * 对logo进行圆角处理并添加边框
     * @param string $logo_image logo二进制数据流
     * @param int $width logo宽度
     * @param int $height logo高度
     * @param int $rounding logo 圆角度
     * @param int $stockWidth 画笔宽度
     * @param null|int $border 边框宽度
     * @param string $save_path 要保存的路径
     * @return string 二进制数据流
     * @throws \ImagickException
     */
    public static function createIcon($logo_image, $width=200, $height=200, $rounding=20, $stockWidth=5, $border=10, $save_path='')
    {
        $x1 = $y1 = (sqrt(2) * $rounding - $rounding) / 2; // 计算圆角框的起点位置
        $x2 = $width - $x1 + $stockWidth/2;
        $y2 = $height - $y1 + $stockWidth/2;  // 计算圆角框的结束位置
        $image_logo = new \Imagick();
        try {
            if(is_file($logo_image) || Str::startsWith($logo_image, 'http')){
                $image_logo->readImage($logo_image);
            }else{
                $image_logo->readImageBlob($logo_image);
            }
        }catch (\Exception $exception){
            try {
                $image_logo->readImageBlob($logo_image);
            }catch (\Exception $exception){
                $image_logo->destroy();
                throw new \Exception('logo错误！');
            }
        }
        $transparent = new \ImagickPixel('rgba(0,0,0,0)'); // 透明
        $white = new \ImagickPixel('white'); // 白色
        $imagick = new \Imagick();
        $imagick->newImage($width, $height, $transparent); // 新建一个背景透明的图片
        $imagick->setImageFormat("png");
        $imagick->setimagematte(true);
        $imagickDraw = new \ImagickDraw();
        $imagickDraw->setStrokeColor($white); // 设置边框颜色
        $imagickDraw->setFillColor($white); // 设置填充颜色
        $imagickDraw->setStrokeOpacity(1);
        $imagickDraw->setStrokeWidth($stockWidth);
        $imagickDraw->roundRectangle($x1, $y1, $x2, $y2, $rounding, $rounding);  // 画一个圆角框
        $imagick->drawImage($imagickDraw);
        $image_logo->setImageFormat('png');
        $image_logo->setImageMatte(true);
        $image_logo->scaleImage($width, $height);
        $image_logo->compositeImage($imagick,\Imagick::COMPOSITE_COPYOPACITY,0, 0, \Imagick::CHANNEL_RGBA);
        if($border){
            $image_logo->scaleImage($width-$border*2, $height-$border*2, true);
            $imagick->compositeImage($image_logo, \Imagick::COMPOSITE_OVER, $border,$border);
            if($save_path){
                $imagick->writeImage($save_path);
            }
            $result = $imagick->getImageBlob();
        }else{
            if($save_path){
                $image_logo->writeImage($save_path);
            }
            $result = $image_logo->getImageBlob();
        }
        $white->destroy();
        $transparent->destroy();
        $imagick->destroy();
        $imagickDraw->destroy();
        $image_logo->destroy();
        return $result;
    }

    /**
     * 对logo进行圆角和border处理 和 createIcon效果相同 处理方式不同
     * @param string $water_path logo路径
     * @param int $width 宽度
     * @param int $height 高度
     * @param int $cornerRadius 圆角度
     * @param int $borderWidth 边框宽度
     * @param string $save_path 保存路径
     * @return string
     * @throws \ImagickException
     */
    public static function createIconT($water_path, $width=100, $height=100, $cornerRadius=10, $borderWidth=5, $save_path='')
    {
        $x1 = $y1 = (sqrt(2) * $cornerRadius - $cornerRadius) / 2; // 计算图片边框圆角到矩形直角之间的距离
        $end_z_b = $borderWidth / 2; // 圆角矩形图片的终点坐标
        $x2 = $x1 + ($x1/2); // 计算logo的放置X轴位置
        $y2 = $y1 + ($y1/2); // 计算logo的放置Y轴位置
        $color_x = new \ImagickPixel; // 圆角矩形边框的线条颜色(白色)
        $color_x->setColor('rgb(255, 255, 255)');
        $color_t = new \ImagickPixel; // 圆形矩形边框的填充颜色 (透明)
        $color_t->setColor('rgba(0, 0, 0, 0)');
        $draw = new \ImagickDraw();// 生成圆角矩形的画布
        $draw->setStrokeColor($color_x); // 设置边框颜色
        $draw->setFillColor($color_t); // 设置填充颜色
        $draw->setStrokeOpacity(1); // 轮廓不透明
        $draw->setStrokeWidth($borderWidth); // 设置圆角矩形的边框宽度
        $draw->roundRectangle($x1, $y1, $width-$end_z_b, $height-$end_z_b, $cornerRadius, $cornerRadius); // 绘制圆角矩形
        $imagick = new \Imagick(); //创建一个新的画布
        $imagick->newImage($width, $height, $color_t); // 设置画布树形 宽 高 背景颜色
        $imagick->setImageFormat("png"); // 设置图片格式
        $imagick_water = new \Imagick($water_path); // 价值logo
        $imagick_water->scaleImage($width-floor($x2 * 2), $height - floor($y2 * 2)); // 对logo进行缩放
        $imagick->compositeImage($imagick_water, \Imagick::COMPOSITE_OVER, $x2, $y2); // 把logo填充到新建的画布
        $imagick->drawImage($draw); // 把圆角矩形绘制到新建的画布覆盖logo的方角
        // 如果保存路径不为空则进行数据保存
        if($save_path){
            $imagick->writeimage($save_path);
        }
        // 返回二进制流
        $result = $imagick->getImageBlob();
        // 关闭画布和图层
        $draw->destroy();
        $imagick_water->destroy();
        $imagick->destroy();
        // 返回二进制流数据
        return $result;
    }

    /**
     * 计算数字倍
     * @param $number
     * @return string
     */
    public static function money($number)
    {
        $d = ['', '万', '亿', '万亿'];
        $index = 0;
        $old_number =  $number;
        if(bccomp($number, 10000, 2) >= 0){
            do{
                $number = bcdiv($number,'10000.00');
                $index ++;
            }while(bccomp($number, 10000, 2) >= 0);
        }
        if($index > count($d)){
            return $old_number;
        }
        return $number . $d[$index];
    }

    /**
     * 数字随机字符串
     * @param int $length
     * @return int|string
     */
    public static function numberRandom($length=10)
    {
        $string = mt_rand(1, 9);
        do{
            $string .= mt_rand(0, 9);
        }while(strlen($string) < $length);
        return $string;
    }

    /**
     * url处理
     * @param string $path 要修改的url链接
     * @param array $query url参数
     * @return string|string[]
     */
    public static function urlHandle($path, $query)
    {
        if($path){
            $url_array = parse_url($path);
            $url_query = Arr::get($url_array, 'query');
            parse_str($url_query, $url_query_array);
            $url_query_array = array_merge($url_query_array, $query);
            $query_string = http_build_query($url_query_array);
            $url_array = array_merge($url_array, ['query' => $query_string]);
            return http_build_url($url_array);
        }
        return 'javascript:alert(\'找不到指定资源!\')';
    }
}
