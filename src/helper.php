<?php

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

if (!function_exists('app_namespace')) {
    function app_namespace($app_directory): string
    {
        return 'App\\'.implode('\\',
                array_map(
                    function ($name) {
                        return Str::studly($name);
                    },
                    explode(
                        DIRECTORY_SEPARATOR,
                        substr(
                            $app_directory,
                            strlen(app_path().DIRECTORY_SEPARATOR)
                        )
                    )
                )
            );
    }
}

if(!function_exists('array_get')){
    function array_get($array, $key, $default=null){
        try {
            if (is_null($key) || (is_string($key) && !$key)) {
                return $default;
            }
            if (!is_array($array)) {
                return $default;
            }
            $keys = explode('.', $key);
            $count = count($keys);
            if ($count > 1) {
                foreach ($keys as $index => $key) {
                    if (isset($array[$key])) {
                        if (($index + 1) == $count) {
                            return $array[$key];
                        }
                        if (is_array($array[$key])) {
                            $array = $array[$key];
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                }
                return $default;
            }
            if (isset($array[$key])) {
                return $array[$key];
            }
            return $default;
        } catch (Exception $e) {
        }
        return $default;
    }
}

if(!function_exists('aes_encrypt')) {
    function aes_encrypt($value, $serialize=true) {
        try {
            return encrypt($value, $serialize);
        } catch (Exception $e) {
            return null;
        }
    }
}

if (!function_exists('aes_decrypt')) {
    function aes_decrypt($value, $unserialize=true) {
        try {
            return decrypt($value, $unserialize);
        } catch (\Exception $exception) {
            return null;
        }
    }
}


if(!function_exists('des_encrypt')){
    /**
     * 加密字符串
     *
     * @return string
     */
    function des_encrypt($str, $passphrase='encrypt_and_decode')
    {
        return base64_encode(openssl_encrypt($str, 'DES-ECB', md5($passphrase)));
    }
}

if(!function_exists('des_decrypt')) {
    /**
     * 解密字符串
     * @param $str
     *
     * @return false|string
     */
    function des_decrypt($str, $passphrase='encrypt_and_decode')
    {
        return openssl_decrypt(base64_decode($str), 'DES-ECB', md5($passphrase));
    }
}

if(!function_exists('str_finish')){
    function str_finish(string $string, string $finishString){
        if(strrpos($string, $finishString) != mb_strlen($string)-1){
            return $string . $finishString;
        }
        return $string;
    }
}

if(!function_exists('str_start')){
    function str_start(string $string, string $startString){
        if(strpos($string, $startString) !== 0){
            return $startString . $string;
        }
        return $string;
    }
}

if(!function_exists('http_build_url')){
    function http_build_url($url_arr){
        try {
            if(!empty($url_arr['scheme']) && !empty($url_arr['host'])){
                $new_url = $url_arr['scheme'] . "://".$url_arr['host'];
                if(!empty($url_arr['port']))
                    $new_url = $new_url.":".$url_arr['port'];
                if(!empty($url_arr['path']))
                    $new_url = $new_url . $url_arr['path'];
                if(!empty($url_arr['query']))
                    $new_url = $new_url . "?" . $url_arr['query'];
                if(!empty($url_arr['fragment']))
                    $new_url = $new_url . "#" . $url_arr['fragment'];
                return $new_url;
            }
        }catch (\Exception $exception){
        }
        return '';
    }
}

if(!function_exists('trim_c')){
    /**
     * 去除字符串前后空格及逗号并以英文逗号分割字符串
     * @param $value string 要分割的字符串
     * @return string
     */
    function trim_c($value){
        return trim($value, " \t\n\r\0\x0B,");
    }
}

if(!function_exists('date_array')){
    /**
     * 生成日期数组
     *
     * @param string $start 开始时间
     * @param string $end 结束时间
     * @param string $format 生成的时间格式
     * @param int $interval 生成的时间间隔(默认一天)
     *
     * @return array|false[]|string[]
     */
    function date_array(string $start, string $end, string $format = 'Y-m-d', int $interval = 86400)
    {
        $dates = range(strtotime($start), strtotime($end), $interval);

        return array_map(function ($_date) use ($format) {
            return date($format, $_date);
        }, $dates);
    }
}

if(!function_exists('explode_id')){
    /**
     * 去除字符串前后空格及逗号并以英文逗号分割字符串
     * @param $value string 要分割的字符串
     * @return array
     */
    function explode_id($value){
        $_tmp = explode(',', trim_c($value));
        if (!$_tmp) {
            return [];
        }
        $result = [];
        for ($i = 0; $i < count($_tmp); $i++) {
            $result[] = intval($_tmp[$i]);
        }
        return array_unique(array_filter($result));
    }
}

if (!function_exists('encode_quotes')) {
    /**
     * 把引号字符串转义为html转义字符
     * @param $val
     * @return string|string[]
     */
    function encode_quotes($val) {
        return str_replace("'", '&#39;', str_replace('"', '&#34;', $val));
    }
}

if (!function_exists('decode_quotes')) {
    /**
     * 把html转义引号转换为正常的引号字符串
     * @param $val
     * @return string|string[]
     */
    function decode_quotes($val) {
        return str_replace('&#39;', "'", str_replace('&#34;', '"', $val));
    }
}

if (!function_exists('urlHandle')) {
    function url_handle($path, $query)
    {
        if($path){
            $url_array = parse_url($path);
            $url_query = array_get($url_array, 'query');
            parse_str($url_query, $url_query_array);
            $url_query_array = array_merge($url_query_array, $query);
            $query_string = http_build_query($url_query_array);
            $url_array = array_merge($url_array, ['query' => $query_string]);
            return http_build_url($url_array);
        }
        return null;
    }
}


if (!function_exists('vueUrlHandle')) {
    function vue_url_handle($path, $query)
    {
        if($path){
            $url_array = parse_url($path);
            $url_query = array_get($url_array, 'query');
            parse_str($url_query, $url_query_array);
            $url_query_array = array_merge($url_query_array, $query);
            $query_string = http_build_query($url_query_array);
            $url_array = array_merge($url_array, ['query' => $query_string]);
            return http_build_vue_url($url_array);
        }
        return null;
    }
}

if(!function_exists('http_build_vue_url')){
    function http_build_vue_url($url_arr){
        try {
            if(!empty($url_arr['scheme']) && !empty($url_arr['host'])){
                $new_url = $url_arr['scheme'] . "://".$url_arr['host'];
                if(!empty($url_arr['port']))
                    $new_url = $new_url.":".$url_arr['port'];
                if(!empty($url_arr['path']))
                    $new_url = $new_url . $url_arr['path'];
                if(!empty($url_arr['fragment']))
                    $new_url = $new_url . "#" . $url_arr['fragment'];
                if(!empty($url_arr['query']))
                    $new_url = $new_url . "?" . $url_arr['query'];
                return $new_url;
            }
        }catch (\Exception $exception){
        }
        return '';
    }
}

if (!function_exists('uniqueId')) {
    function uniqueId(){
        return strtoupper(md5(uniqid(microtime(true),true)));
    }
}

if (!function_exists('imageRound')) {
    /**
     * 把正方形图片裁剪为圆形
     * @param string $path 原图片路径或者二进制字符串
     * @param string $save_path 裁剪完图片保存路径，如果为空则返回图片的二进制流数据
     * @return bool|false|string 如果返回值为false 则裁剪图片失败否则裁剪完成，$save_path为空则返回二进制数据，否则为true
     */
    function imageRound($path, $save_path = '')
    {
        if (@is_file($path)) {
            // 如果传入的是文件路径则打开文件再创建图片
            $src = imagecreatefromstring(file_get_contents($path));
        } else {
            // 通过二进制字符串创建图片对象
            $src = @imagecreatefromstring($path);
        }
        if (!$src) {
            return false;
        }
        $w = imagesx($src);
        $h = imagesy($src);
        // 新建一个图像
        $new_pic = imagecreatetruecolor($w, $h);
        // 关闭图像混色
        imagealphablending($new_pic, false);
        // 设置图片保存透明通道
        imagesavealpha($new_pic, true);
        // 设置图片透明底色
        $transparent = imagecolorallocatealpha($new_pic, 0, 0, 0, 127);
        // 获取圆形半径
        $r = $w / 2;
        for ($x = 0; $x < $w; $x++) {
            for ($y = 0; $y < $h; $y++) {
                // 获取原图片指定坐标点的像素值
                $c = imagecolorat($src, $x, $y);
                $_x = $x - $w / 2;
                $_y = $y - $h / 2;
                if ((($_x * $_x) + ($_y * $_y)) < ($r * $r)) {
                    // 设置像素颜色值为原图片像素的颜色值
                    imagesetpixel($new_pic, $x, $y, $c);
                } else {
                    // 设置像素颜色值为透明
                    imagesetpixel($new_pic, $x, $y, $transparent);
                }
            }
        }
        $data = false;
        if (!$save_path) {
            // 如果保存路径为空则创建临时文件
            $save_path = tempnam(sys_get_temp_dir(), 'image_round');
            if (imagepng($new_pic, $save_path)) {
                // 如果图片保存到临时文件成功则读取图片的二进制数据
                $data = file_get_contents($save_path);
                // 删除临时文件
                @unlink($save_path);
            }
        } else {
            // 保存图片到指定路径
            $data = imagepng($new_pic, $save_path);
        }
        // 销毁裁剪为的图片对象
        imagedestroy($new_pic);
        // 销毁原图片对象
        imagedestroy($src);
        return $data;
    }
}

if (!function_exists('imageChangeSize')) {
    /**
     * 修改图片大小
     * @param string $path 图片路径或者图片二进制数据
     * @param int $w 图片宽度
     * @param int $h 图片高度
     * @param string $save_path 保存路径，如果为空则返回图片二进制数据
     * @return bool|false|string
     */
    function imageChangeSize($path, $w, $h, $save_path=''){
        if (@is_file($path)) {
            // 如果传入的是文件路径则打开文件再创建图片
            $path = file_get_contents($path);
        }
        if (!$w || !$h) {
            return $path;
        }
        // 通过二进制字符串创建图片对象
        $image = @imagecreatefromstring($path);
        if (!$image) {
            return false;
        }
        $new_pic = imagecreatetruecolor($w, $h);
        $s_w = imagesx($image);
        $s_h = imagesy($image);
        // 关闭图像混色
        imagealphablending($new_pic, false);
        // 设置图片保存透明通道
        imagesavealpha($new_pic, true);
        imagecopyresampled($new_pic, $image,0, 0, 0,0, $w, $h, $s_w, $s_h);
        $data = false;
        if (!$save_path) {
            // 如果保存路径为空则创建临时文件
            $save_path = tempnam(sys_get_temp_dir(), 'image_change_size');
            if (imagepng($new_pic, $save_path)) {
                // 如果图片保存到临时文件成功则读取图片的二进制数据
                $data = file_get_contents($save_path);
                // 删除临时文件
                @unlink($save_path);
            }
        } else {
            // 保存图片到指定路径
            $data= imagepng($new_pic, $save_path);
        }
        imagedestroy($image);
        imagedestroy($new_pic);
        return $data;
    }
}

if (!function_exists('addImageWater')) {
    /**
     * @param string $dist_path 要添加水印的图片或路径
     * @param string $water_path 水印图片或路径
     * @param int $x 水印放置X轴位置
     * @param int $y 水印放置Y轴位置
     * @param null|int $water_width 水印图片宽
     * @param null|int $water_height 水印图片高
     * @param string $save_path 加我水印后保存路径如果为空返回图片二进制字符串
     * @return bool|false|string
     */
    function addImageWater($dist_path, $water_path, $x, $y, $water_width=null, $water_height=null, $save_path=''){
        if (@is_file($dist_path)) {
            // 如果传入的是文件路径则打开文件再创建图片
            $dist_image = imagecreatefromstring(file_get_contents($dist_path));
        } else {
            // 通过二进制字符串创建图片对象
            $dist_image = @imagecreatefromstring($dist_path);
        }
        if (!$dist_image) {
            return false;
        }
        $water_image_str = imageChangeSize($water_path, $water_width, $water_height);
        if (!$water_image_str) {
            return false;
        }
        $water_image = imagecreatefromstring($water_image_str);
        if (!$water_image) {
            return false;
        }
        $water_width = imagesx($water_image);
        $water_height = imagesy($water_image);
        imagecopy($dist_image, $water_image, $x, $y, 0, 0, $water_width, $water_height);
        $data = false;
        if (!$save_path) {
            // 如果保存路径为空则创建临时文件
            $save_path = tempnam(sys_get_temp_dir(), 'image_change_size');
            if (imagepng($dist_image, $save_path)) {
                // 如果图片保存到临时文件成功则读取图片的二进制数据
                $data = file_get_contents($save_path);
                // 删除临时文件
                @unlink($save_path);
            }
        } else {
            // 保存图片到指定路径
            $data= imagepng($dist_image, $save_path);
        }
        imagedestroy($dist_image);
        imagedestroy($water_image);
        return $data;
    }
}

if (!function_exists('changeMiniProgramLogo')) {
    /**
     * 修改小程序二维码的logo
     * @param string $path 小程序码图片或者二进制数据
     * @param string $logo_path logo图片地址或者二进制数据
     * @param string $save_path 如果为空则返回图片二进制流字符串
     * @return bool|false|string
     */
    function changeMiniProgramLogo($path, $logo_path, $save_path='') {
        if (@is_file($path)){
            [$w, $h, $type] = getimagesize($path);
        } else {
            [$w, $h, $type] = getimagesizefromstring($path);
        }
        if (!$w) {
            return false;
        }
        // 计算logo占二维码的宽度和高度
        $logo_w = $w / 2.2;
        // 计算logo在图片上的位置
        $x = ($w - $logo_w) / 2;
        $logo = imageRound($logo_path);
        return addImageWater($path, $logo, $x, $x, $logo_w, $logo_w, $save_path);
    }
}

if (!function_exists('dateArray')) {
    /**
     * @param int $unix_timestamp
     * @param int $day 大于0 为传入时间往后多少天  小于0为传入时间戳往前多少天
     * @param false $return_unix_timestamp
     */
    function dateArray($unix_timestamp, $day, $format='Y-m-d H:i:s', $return_unix_timestamp=false) {
        $date = [];
        if ($day < 0) {
            for ($i = abs($day); $i > 0; $i--) {
                if ($return_unix_timestamp) {
                    $date[] = $unix_timestamp - $i * 86400;
                } else {
                    $date[] = date($format, $unix_timestamp - $i * 86400);
                }
            }
        }
        else if ($day > 0) {
            for ($i = 0; $i < $day; $i++) {
                if ($return_unix_timestamp) {
                    $date[] = $unix_timestamp + $i * 86400;
                } else {
                    $date[] = date($format, $unix_timestamp + $i * 86400);
                }
            }
        } else {
            if ($return_unix_timestamp) {
                return [$unix_timestamp];
            } else {
                return [date($format, $unix_timestamp)];
            }
        }
        return $date;

    }
}

if (!function_exists('getCsv')) {
    function getCsv($csv) {
        $data_str = file_get_contents($csv);
        $str_encode = mb_convert_encoding($data_str, "utf-8", ["ASCII", "UTF-8","GB2312","GBK","BIG5"]);
        unset($data_str);
        if ($str_encode) {
            file_put_contents($csv, $str_encode);
        }
        unset($str_encode);
        $file = fopen($csv, 'rb');
        if (!$file) {
            return [];
        }
        $rows = [];
        while (!feof($file)) {
            $t_c = fgets($file);
            $t_d = explode(',', $t_c);
            if ($t_d) {
                $t_d_1 = [];
                foreach ($t_d as $s) {
                    $t_d_1[] = trim($s, " \t\n\r\0\x0B\"\'\\");
                }
                $rows[] = $t_d_1;
                unset($t_d_1);
            }
            unset($t_d);
        }
        fclose($file);
        return $rows;
    }
}

if (!function_exists('str_limit')) {
    function str_limit($str, $limit = null) {
        if (!$limit) {
            return $str;
        }
        $limit = intval($limit);
        if (mb_strlen($str) > $limit) {
            return mb_substr($str, 0, $limit) . '...';
        }
        return $str;
    }
}

if (!function_exists('resource_full_path')) {
    function resource_full_path($path, $server=null) {
        if (!$path) {
            return '';
        }
        if (url()->isValidUrl($path) || mb_strpos($path, 'data:image') === 0) {
            $src = $path;
        } elseif ($server) {
            $src = rtrim($server, '/').'/'.ltrim($path, '/');
        } else {
            $disk = config('admin.upload.disk');

            if (config("filesystems.disks.{$disk}")) {
                $src = Storage::disk($disk)->url($path);
            } else {
                $src = '';
            }
        }
        $scheme = 'http:';
        if (config('admin.https', false)) {
            $scheme = 'https:';
        }
        $src = preg_replace('/^http[s]{0,1}:/', $scheme, $src, 1);
        return $src;
    }
}
