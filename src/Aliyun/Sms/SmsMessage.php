<?php


namespace Myaccountl\LaravelTools\Aliyun\Sms;


use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class SmsMessage
{

    /**
     * Handle the event.
     * 调用阿里云短信服务接口发送短信
     * Create a new event instance.
     * @param string $accessKeyId
     * @param string $accessSecret
     * @param string $name 短信签名
     * @param array $to 接收短信的用户数组 ['phone1', 'phone2']
     * @param string $tpl 发送模板  模板ID: SMS_XXXXXXXXX
     * @param array $data 和接收短信用户对应的模板数据[[phone1模板变量数据], [phone2模板变量数据]]，
     *                    如果是给多个用户发送同一个数据则只用传递一个数据 [[模板变量数据]]
     * @return bool
     */
    public static function send($accessKeyId, $accessSecret, $name, $to, $tpl, $data)
    {
        if(count($data) == 1){
            return self::sendSms($accessKeyId, $accessSecret, $name, $to, $tpl, $data);
        }else if(count($data) == count($to)){
            return self::sendBatchSms($accessKeyId, $accessSecret, $name, $to, $tpl, $data);
        }
        return false;
    }

    /**
     * 给多个用户发送相同的信息 或者单个用户发送一条信息
     * @param string $accessKeyId
     * @param string $accessSecret
     * @param $name
     * @param $to
     * @param $tpl
     * @param $data
     * @return bool
     */
    private static function sendSms(string $accessKeyId, string $accessSecret, $name, $to, $tpl, $data){
        $query = [
            'RegionId' => "cn-hangzhou",
            'PhoneNumbers' => implode(',', $to),
            'SignName' => $name,
            'TemplateCode' => $tpl,
            'TemplateParam' => json_encode($data[0]),
        ];
        Log::debug('sendSms', $query);
        return self::sendApi('SendSms',$accessKeyId, $accessSecret,  $query);
    }

    /**
     * 给多个用户发送不用的信息 只能是同一个模板 不同的模板数据
     * @param string $accessKeyId
     * @param string $accessSecret
     * @param $name
     * @param $to
     * @param $tpl
     * @param $data
     * @return bool
     */
    private static function sendBatchSms(string $accessKeyId, string $accessSecret, $name, $to, $tpl, $data){
        $sign_name = [];
        for($i=0; $i< count($to); $i++){
            $sign_name[] = $name;
        }

        $query = [
            'RegionId' => "cn-hangzhou",
            'PhoneNumberJson' => json_encode($to),
            'SignNameJson' => json_encode($sign_name),
            'TemplateCode' => $tpl,
            'TemplateParamJson' => json_encode($data),
        ];
        Log::debug('sendBatchSms: ', $query);
        return self::sendApi('SendBatchSms', $accessKeyId, $accessSecret, $query);
    }

    /**
     * 发送短信
     * @param string $action
     * @param string $accessKeyId
     * @param string $accessSecret
     * @param array $data
     * @return bool
     */
    private static function sendApi(string  $action, string $accessKeyId, string $accessSecret,  array  $data){
        if (!$accessKeyId || !$accessSecret) {
            Log::error('Access Key 和 Access Secret 不能为空！');
            return false;
        }
        try {
            AlibabaCloud::accessKeyClient($accessKeyId, $accessSecret)
                ->regionId('cn-hangzhou')
                ->asDefaultClient();
            $result = AlibabaCloud::rpc()
                ->product('Dysmsapi')
                // ->scheme('https') // https | http
                ->version('2017-05-25')
                ->action($action)
                ->method('POST')
                ->host('dysmsapi.aliyuncs.com')
                ->options([
                    'query' => $data,
                ])
                ->request();
            $result_data = $result->toArray();
            Log::debug('send', $result_data);
            return Arr::get($result_data, 'Code') == 'OK'?true:false;
        } catch (ClientException $e) {
            Log::error($e->getErrorMessage());
        } catch (ServerException $e) {
            Log::error($e->getErrorMessage());
        } catch (\Exception $exception){
            Log::error($exception->getMessage());
        }
        return false;
    }
}
