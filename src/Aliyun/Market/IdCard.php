<?php


namespace Myaccountl\LaravelTools\Aliyun\Market;


use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;

class IdCard
{
    private static $url = 'http://idcert.market.alicloudapi.com/idcard';
    public static function verifyIdCard($app_code, $name, $id_card)
    {
        $response = Http::withHeaders([
            'Authorization' => 'APPCODE ' . $app_code
        ])->get(self::$url, ['idCard' => $id_card, 'name' => $name]);
        $res = $response->json();
        if ($res && (Arr::get($res, 'status', '') == '01' || Arr::get($res, 'status', '') == '202')) {
            return true;
        }
        return false;
    }
}
