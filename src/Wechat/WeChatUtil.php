<?php

namespace Myaccountl\LaravelTools\Wechat;

use EasyWeChat\Factory;
use EasyWeChat\Kernel\Http\StreamResponse;
use Illuminate\Support\Facades\Log;

class WeChatUtil
{
    public static function generateMiniProgramQrcode($scene,  $path='mini_program_qrcode', $page='pages/tab-bar/index'): string
    {
        $app = Factory::miniProgram(config('wechat.mini_program.default'));

        try {
            $data_params = config('wechat.mini_program.qrcode');
            $data_params['page'] = $page;
            // 获取小程序码
            $response = $app->app_code->getUnlimit($scene, $data_params);
            // 文件
            $filename = $scene . '.png';

            if ($response instanceof StreamResponse) {
                // 保存图片
                $response->saveAs(storage_path('app/public/' . $path), $filename);
                // 保存路径
                return  $path . '/' . $filename;
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
        return '';
    }

}
